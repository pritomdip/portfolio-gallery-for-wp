/**
 * CA popup Trigger Admin
 * 
 *
 * Copyright (c) 2019 Pritom Chowdhury Dip
 * Licensed under the GPLv2+ license.
 */

/*jslint browser: true */
/*global jQuery:false */

window.Project = (function (window, document, $, undefined) {
    'use strict';

    var app = {
        initialize: function () {

            app.SelectFieldShowHide();
            app.selectImageShowHide();
            app.selectMsngrStyleShowHide();
            app.urgencytrigger();
            app.urgencySelectedIcon();

            $('#ca_content2_visibility').on('change', app.urgencytrigger);
            $('#ca_content3_visibility').on('change', app.urgencytrigger);   
        	$('.ca-color-field').wpColorPicker();
            $('#ca-select-popup-style').on('change', app.SelectFieldShowHide);
            $('#ca_msg_img_visibility').on('change', app.selectImageShowHide);
            $('#ca_overlay_img_visibility').on('change', app.selectImageShowHide);
            $('.upload_image_button').on('click', app.imageUploadOnClick);
            $('.remove_image_button').on('click', app.imageRemoveOnClick); 
            $('#ca-msg-content-style, #ca-overlay-content-style').on('change', app.selectMsngrStyleShowHide);
            $('#ca_urgency_content1_icon').on('change', app.selectedIconOnChange);          
            $('#ca_urgency_content2_icon').on('change', app.selectedIcon2OnChange);          
            $('#ca_urgency_content3_icon').on('change', app.selectedIcon3OnChange);          

        },
        urgencySelectedIcon:function(){
            var content1_icon = $('#ca_urgency_content1_icon').attr('value');
            $('.ca_content1_icon_placement').addClass(content1_icon);
            var content2_icon = $('#ca_urgency_content2_icon').attr('value');
            $('.ca_content2_icon_placement').addClass(content2_icon);
            var content3_icon = $('#ca_urgency_content3_icon').attr('value');
            $('.ca_content3_icon_placement').addClass(content3_icon);
        },
        selectedIcon2OnChange: function(){
            var b = $(this).attr('value');
            $('.ca_content2_icon_placement').removeClass().addClass('dashicons ca_content2_icon_placement ' + b );
        },
        selectedIcon3OnChange: function(){
            var c = $(this).attr('value');
            $('.ca_content3_icon_placement').removeClass().addClass('dashicons ca_content3_icon_placement ' + c );            
        },
        selectedIconOnChange: function(){
            var a = $(this).attr('value');
            $('.ca_content1_icon_placement').removeClass().addClass('dashicons ca_content1_icon_placement ' + a );
        },
        urgencytrigger:function(){
            var selectedStyle  = $('#ca_content2_visibility :selected').text();
            var selectedStyle2 = $('#ca_content3_visibility :selected').text();
            app.caShowHide( selectedStyle, 'Yes', '#trigger2' );
             app.caShowHide( selectedStyle2, 'Yes', '#trigger3' );
        },
        SelectFieldShowHide: function(){
        	var selectedStyle = $('#ca-select-popup-style :selected').text();

            app.caShowHide( selectedStyle, 'Sticky-bar', '#sticky-bar' );
            app.caShowHide( selectedStyle, 'Sidebar-offer', '#sidebar-offer' );
            app.caShowHide( selectedStyle, 'Sidebar-list', '#sidebar-list' );
            app.caShowHide( selectedStyle, 'Messenger', '#messenger' );
            app.caShowHide( selectedStyle, 'Overlay', '#overlay' );
            app.caShowHide( selectedStyle, 'Urgency-trigger', '#urgency-trigger' );
            app.caShowHide( selectedStyle, 'Html-widgets', '#html-widgets' );
        },
        caShowHide: function( $style, $value, $IdName ){
            if ( $style === $value ) {
                $( $IdName ).show();
            }
            else {
                $( $IdName ).hide();
            }
        },
        selectImageShowHide: function(){
            var selectedStyle  = $('#ca_msg_img_visibility :selected').text();
            var selectedStyle2 = $('#ca_overlay_img_visibility :selected').text();
            app.caShowHide( selectedStyle, 'Yes', '#msg-img' );
            app.caShowHide( selectedStyle2, 'Yes', '#overlay-img' );
        },
        selectMsngrStyleShowHide: function(){
            var selectedStyle = $('#ca-msg-content-style :selected').text();
            app.caShowHide(selectedStyle, 'List-View', '#list-view');
            app.caShowHide(selectedStyle, 'Message-Box', '#message-box');

            var overlayStyle = $('#ca-overlay-content-style :selected').text();
            app.caShowHide( overlayStyle, 'List-View', '#overlay-list-view' );
            app.caShowHide( overlayStyle, 'Message-Box', '#overlay-message-box' );
        },
        imageUploadOnClick: function(){
            var send_attachment_bkp = wp.media.editor.send.attachment;
            var button = $(this);
            wp.media.editor.send.attachment = function(props, attachment) {
                $(button).parent().prev().attr('src', attachment.url);
                $(button).prev().val(attachment.id);
                wp.media.editor.send.attachment = send_attachment_bkp;
            };
            wp.media.editor.open(button);
            return false;
        },
        imageRemoveOnClick: function(){
            var answer = confirm('Are you sure?');
            if (answer === true) {
                var src = $(this).parent().prev().attr('data-src');
                $(this).parent().prev().attr('src', src);
                $(this).prev().prev().val('');
            }
            return false;
        }
    };
 
    $(document).ready(app.initialize);
    
    return app; 
})(window, document, jQuery);