<?php

namespace Pritom\PortfolioGalleryForWp\Admin;

use Pritom\PortfolioGalleryForWp\Admin\Settings_API;

class Settings {
	private $settings_api;

	function __construct() {
		$this->settings_api = new Settings_API();
		add_action('admin_init', array($this, 'admin_init'));
		add_action('admin_menu', array($this, 'admin_menu'));
	}

	function admin_init() {
		//set the settings
		$this->settings_api->set_sections($this->get_settings_sections());
		$this->settings_api->set_fields($this->get_settings_fields());

		//initialize settings
		$this->settings_api->admin_init();
	}

	function get_settings_sections() {
		$sections = array(
			array(
				'id'    => 'pgw_portfolio_settings',
				'title' => __( 'Portfolio Settings', 'event-calendar-pro' )
			),
		);

		return apply_filters( 'pgw_portfolio_settings_sections', $sections );
	}

	/**
	 * Returns all the settings fields
	 *
	 * @return array settings fields
	 */
	function get_settings_fields() {

		$settings_fields = array(

			'pgw_portfolio_settings' => array(

				array(
					'name'    => 'enable_stylesheet',
					'label'   => __('Enable Stylesheet', 'portfolio-gallery-for-wp'),
					'desc'    => sprintf('<span class="howto">%s</span>', __('Enable / Disable plugin default stylesheet.', 'portfolio-gallery-for-wp')),
					'type'    => 'checkbox',
					'default' => 'checked',
					'class'   => 'enable_stylesheet_check',
				),

				/**
				 * ====================
				 * Shape
				 * ====================
				 */

				array(
					'name'  => 'shape_style_heading',
					'label' => __('Shape Style', 'portfolio-gallery-for-wp'),
					'type'  => 'heading',
				),

				array(
					'name'    => 'shape_style',
					'label'   => __('Shape Style', 'portfolio-gallery-for-wp'),
					'desc'    => __('Attribute Shape Style.', 'portfolio-gallery-for-wp'),
					'type'    => 'radio',
					'options' => array(
						'round'  => 'Round',
						'square' => 'Square'
					),
					'default' => 'round',

				),

				
				array(
					'name'    => 'width',
					'label'   => __('Width', 'portfolio-gallery-for-wp'),
					'desc'    => __('Variation Item Width.', 'portfolio-gallery-for-wp'),
					'type'    => 'text',
					'default' => '30px',
				),

				array(
					'name'    => 'height',
					'label'   => __('Height', 'portfolio-gallery-for-wp'),
					'desc'    => __('Variation Item Height.', 'portfolio-gallery-for-wp'),
					'type'    => 'text',
					'default' => '30px',
				),

				/**
				 * =====================
				 * Tooltip
				 * =====================
				 */


				array(
					'name'  => 'tooltip_heading',
					'label' => __('Tooltip', 'portfolio-gallery-for-wp'),
					'type'  => 'heading',
				),

				array(
					'name'    => 'enable_tooltip',
					'label'   => __('Enable Tooltip', 'portfolio-gallery-for-wp'),
					'desc'    => sprintf('<span class="howto">%s</span>', __('Enable / Disable plugin default tooltip on each product attribute.', 'portfolio-gallery-for-wp')),
					'type'    => 'checkbox',
					'default' => 'checked',
				),

				array(
					'name'    => 'font_size',
					'label'   => __('Tooltip Font Size', 'portfolio-gallery-for-wp'),
					'desc'    => __('Tooltip Font Size.', 'portfolio-gallery-for-wp'),
					'type'    => 'text',
					'default' => '15px',
				),

				array(
					'name'    => 'tooltip_bg_color',
					'label'   => __('Tooltip Background Color', 'portfolio-gallery-for-wp'),
					'type'    => 'color',
					'default' => '#555',
				),

				array(
					'name'    => 'tooltip_text_color',
					'label'   => __('Tooltip Text Color', 'portfolio-gallery-for-wp'),
					'type'    => 'color',
					'default' => '#fff',
				),

				/**
				 * =====================
				 * Border
				 * =====================
				 */

				array(
					'name'  => 'border_heading',
					'label' => __('Border', 'portfolio-gallery-for-wp'),
					'type'  => 'heading',
				),

				array(
					'name'    => 'border',
					'label'   => __('Border Style', 'portfolio-gallery-for-wp'),
					'desc'    => __('Enable/Disable Border.', 'portfolio-gallery-for-wp'),
					'type'    => 'radio',
					'options' => array(
						'enable'  => 'Enable',
						'disable' => 'Disable'
					),
					'default' => 'enable',
				),

				array(
					'name'    => 'border_color',
					'label'   => __('Border Color', 'portfolio-gallery-for-wp'),
					'desc'    => __('Default border color.', 'portfolio-gallery-for-wp'),
					'type'    => 'color',
					'default' => '#555',
				),

			),
		);

		return apply_filters('pgw_portfolio_settings_fields', $settings_fields);
	}

	/**
	 * Add Portfolio Gallary settings sub menu to Portfolio admin menu
	 *
	 * @since 1.0.0
	 */

	function admin_menu() {

		add_submenu_page(
			'edit.php?post_type=photo_gallery',
			__('Portfolio Settings', 'portfolio-gallery-for-wp'),
			__('Portfolio Settings', 'portfolio-gallery-for-wp'),
			'manage_options',
			'portfolio-gallary-sorting',
			array($this, 'settings_page')
		);

	}

	/**
	 * Menu page for Portfolio Gallery sub menu
	 *
	 * @since 1.0.0
	 */

	function settings_page() {
		echo '<div class="wrap">';
		echo sprintf("<h2>%s</h2>", __('Portfolio Gallary Settings', 'portfolio-gallery-for-wp'));
		$this->settings_api->show_settings();
		echo '</div>';

	}
}




