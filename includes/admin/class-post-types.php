<?php

namespace Pritom\PortfolioGalleryForWp\Admin;

class PostTypes {
    /**
     * PostTypes constructor.
     */
    public function __construct() {
        add_action( 'init', array( $this, 'register_post_types' ) );
        add_action( 'init', array( $this, 'register_taxonomies' ) );
    }

    /**
     * Register custom post types
     */
    public function register_post_types() {
        register_post_type( 'photo_gallery', array(
            'labels'              => $this->get_posts_labels( 'Portfolio', __( 'Portfolio', 'portfolio-gallery-for-wp' ), __( 'Photogallary Gallery', 'portfolio-gallery-for-wp' ) ),
            'hierarchical'        => false,
            'supports'            => array(  'title', 'editor', 'thumbnail' ),
            'public'              => true,
            'show_ui'             => true,
            'show_in_menu'        => true,
            'show_in_nav_menus'   => true,
            'menu_position'       => 5,
            'menu_icon'           => 'dashicons-admin-tools',
            'publicly_queryable'  => true,
            'exclude_from_search' => false,
            'has_archive'         => true,
            'query_var'           => true,
            'can_export'          => true,
            'rewrite'             => true,
            'capability_type'     => 'post',
        ) );

    }

	/**
	 * Register custom taxonomies
	 *
	 * @since 1.0.0
	 */
    public function register_taxonomies() {
        register_taxonomy( 'photo_gallery_category', array( 'photo_gallery' ), array(
            'hierarchical'      => true,
            'labels'            => $this->get_posts_labels( 'Portfolio Category', __( 'Portfolio Category', 'portfolio-gallery-for-wp' ), __( 'Portfolio Categories', 'portfolio-gallery-for-wp' ) ),
            'show_ui'           => true,
            'show_admin_column' => true,
            'query_var'         => true,
        ) );

    }

	/**
	 * Get all labels from post types
	 *
	 * @param $menu_name
	 * @param $singular
	 * @param $plural
	 *
	 * @return array
	 * @since 1.0.0
	 */
    protected static function get_posts_labels( $menu_name, $singular, $plural ) {
        $labels = array(
            'name'               => $singular,
            'all_items'          => sprintf( __( "All %s", 'portfolio-gallery-for-wp' ), $plural ),
            'singular_name'      => $singular,
            'add_new'            => sprintf( __( 'New %s', 'portfolio-gallery-for-wp' ), $singular ),
            'add_new_item'       => sprintf( __( 'Add New %s', 'portfolio-gallery-for-wp' ), $singular ),
            'edit_item'          => sprintf( __( 'Edit %s', 'portfolio-gallery-for-wp' ), $singular ),
            'new_item'           => sprintf( __( 'New %s', 'portfolio-gallery-for-wp' ), $singular ),
            'view_item'          => sprintf( __( 'View %s', 'portfolio-gallery-for-wp' ), $singular ),
            'search_items'       => sprintf( __( 'Search %s', 'portfolio-gallery-for-wp' ), $plural ),
            'not_found'          => sprintf( __( 'No %s found', 'portfolio-gallery-for-wp' ), $plural ),
            'not_found_in_trash' => sprintf( __( 'No %s found in Trash', 'portfolio-gallery-for-wp' ), $plural ),
            'parent_item_colon'  => sprintf( __( 'Parent %s:', 'portfolio-gallery-for-wp' ), $singular ),
            'menu_name'          => $menu_name,
        );

        return $labels;
    }

	/**
	 * Get all labels from taxonomies
	 *
	 * @param $menu_name
	 * @param $singular
	 * @param $plural
	 *
	 * @return array
	 * @since 1.0.0
	 */
    protected static function get_taxonomy_label( $menu_name, $singular, $plural ) {
        $labels = array(
            'name'              => sprintf( _x( '%s', 'taxonomy general name', 'portfolio-gallery-for-wp' ), $plural ),
            'singular_name'     => sprintf( _x( '%s', 'taxonomy singular name', 'portfolio-gallery-for-wp' ), $singular ),
            'search_items'      => sprintf( __( 'Search %', 'portfolio-gallery-for-wp' ), $plural ),
            'all_items'         => sprintf( __( 'All %s', 'portfolio-gallery-for-wp' ), $plural ),
            'parent_item'       => sprintf( __( 'Parent %s', 'portfolio-gallery-for-wp' ), $singular ),
            'parent_item_colon' => sprintf( __( 'Parent %s:', 'portfolio-gallery-for-wp' ), $singular ),
            'edit_item'         => sprintf( __( 'Edit %s', 'portfolio-gallery-for-wp' ), $singular ),
            'update_item'       => sprintf( __( 'Update %s', 'portfolio-gallery-for-wp' ), $singular ),
            'add_new_item'      => sprintf( __( 'Add New %s', 'portfolio-gallery-for-wp' ), $singular ),
            'new_item_name'     => sprintf( __( 'New % Name', 'portfolio-gallery-for-wp' ), $singular ),
            'menu_name'         => __( $menu_name, 'portfolio-gallery-for-wp' ),
        );

        return $labels;
    }
}
