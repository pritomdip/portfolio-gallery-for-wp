<?php
namespace pritom\PortfolioGalleryForWp;

class Install {
    /**
     * Install constructor.
     */
    public function __construct() {
       // add_action( 'init', array( __CLASS__, 'install' ) );
//        add_filter( 'cron_schedules', array( __CLASS__, 'cron_schedules' ) );
    }

    public static function install() {
        if ( get_option( 'portfolio_gallery_for_wp_install_date' ) ) {
            return;
        }

        if ( ! is_blog_installed() ) {
            return;
        }

        // Check if we are not already running this routine.
        if ( 'yes' === get_transient( 'portfolio_gallery_for_wp_installing' ) ) {
            return;
        }

        //self::create_options();
        //self::create_tables();
        //self::create_cron_jobs();
        
        delete_transient( 'portfolio_gallery_for_wp_installing' );
    }

    /**
     * Save option data
     */
    public static function create_options() {
        //save db version
        update_option( 'wpcp_version', PGW_VERSION );

        //save install date
        update_option( 'portfolio_gallery_for_wp_install_date', current_time( 'timestamp' ) );
    }

    private static function create_tables() {
        global $wpdb;
        $collate = '';
        if ( $wpdb->has_cap( 'collation' ) ) {
            if ( ! empty( $wpdb->charset ) ) {
                $collate .= "DEFAULT CHARACTER SET $wpdb->charset";
            }
            if ( ! empty( $wpdb->collate ) ) {
                $collate .= " COLLATE $wpdb->collate";
            }
        }
        $table_schema = [
            "CREATE TABLE IF NOT EXISTS `{$wpdb->prefix}portfolio_gallery_for_wp` (
                `faker_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
                `faker_name` varchar(255) NOT NULL,
                `faker_city` varchar(255) NOT NULL,
                `product_name` varchar(255) NOT NULL,
                `product_image` varchar(255) NOT NULL,
                PRIMARY KEY (`faker_id`)
            ) $collate;",
        ];
        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
        foreach ( $table_schema as $table ) {
            dbDelta( $table );
        }
    }

    /**
     * Add more cron schedules.
     *
     * @param  array $schedules List of WP scheduled cron jobs.
     *
     * @return array
     */
    public static function cron_schedules( $schedules ) {
        $schedules['monthly'] = array(
            'interval' => 2635200,
            'display'  => __( 'Monthly', 'portfolio-gallery-for-wp' ),
        );

        return $schedules;
    }

    /**
     * Create cron jobs (clear them first).
     */
    private static function create_cron_jobs() {
        wp_clear_scheduled_hook( 'portfolio_gallery_for_wp_daily_cron' );
        wp_schedule_event( time(), 'daily', 'portfolio_gallery_for_wp_daily_cron' );
    }


}

new Install();
